// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "MarsGameGameMode.generated.h"
/**
 * 
 */
UCLASS()
class MARSGAME_API AMarsGameGameMode : public AGameMode
{
	GENERATED_BODY()
	
	AMarsGameGameMode(const FObjectInitializer& ObjectInitializer);

	virtual void StartPlay() override;
	
};
