// Fill out your copyright notice in the Description page of Project Settings.

#include "MarsGame.h"
#include "MarsGameGameMode.h"
#include "MyCharacter.h"

AMarsGameGameMode::AMarsGameGameMode(const FObjectInitializer& ObjectInitializer):Super(ObjectInitializer)
{
	DefaultPawnClass = AMyCharacter::StaticClass();
}

void AMarsGameGameMode::StartPlay()
{
	Super::StartPlay();

	StartMatch();

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("HELLO WORLD"));
	}
}
